# based on https://registry.hub.docker.com/u/samtstern/android-sdk/dockerfile/ with openjdk-8
FROM openjdk:8

MAINTAINER FUJI Goro <g.psy.va+github@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -yq libc6:i386 libstdc++6:i386 zlib1g:i386 libncurses5:i386 --no-install-recommends && \
    apt-get clean

RUN mkdir /usr/local/android-sdk-linux
ENV ANDROID_HOME /usr/local/android-sdk-linux
ENV ANDROID_SDK /usr/local/android-sdk-linux
ADD android-sdk-license $ANDROID_HOME/licenses/
RUN cat $ANDROID_HOME/licenses/android-sdk-license

# Download and untar SDK
ENV ANDROID_SDK_URL https://dl.google.com/android/repository/tools_r25.1.7-linux.zip
RUN curl -sS "${ANDROID_SDK_URL}" > sdk.zip
RUN unzip sdk.zip -d /usr/local/android-sdk-linux
RUN rm -f sdk.zip
ENV PATH ${ANDROID_HOME}/tools:$ANDROID_HOME/platform-tools:$PATH

# Install Android SDK components

# License Id: android-sdk-license-ed0d0a5b
ENV ANDROID_COMPONENTS platform-tools,build-tools-25.0.2,android-25,build-tools-27.0.2,android-27
# License Id: android-sdk-license-5be876d5
ENV GOOGLE_COMPONENTS extra-android-m2repository,extra-google-m2repository

RUN echo y | android update sdk --no-ui --all --filter "${ANDROID_COMPONENTS}" ; \
    echo y | android update sdk --no-ui --all --filter "${GOOGLE_COMPONENTS}"

# Support Gradle
ENV TERM dumb
